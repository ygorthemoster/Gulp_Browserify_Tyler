# React Tutorial Pt 2: Building React Applications with Gulp and Browserify.
This is an implementation of two tutorials by Tyler, both are freely avaliable at: https://tylermcginnis.com/react-js-tutorial-pt-2-building-react-applications-with-gulp-and-browserify/, this is an aticle on using a gulpfile to build a react app, this is the final product of said tutorial

## Pre-requisites
To test this project you'll need:
- [git](https://git-scm.com/)
- npm available when installing [Node.js](https://nodejs.org/en/)
- [gulp](http://gulpjs.com/)

## Installing

The install process can be done by typing the following on a terminal:

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Gulp_Browserify_Tyler.git
cd Gulp_Browserify_Tyler
npm install
gulp production
```

## Running
you'll find the built index.html inside the build folder

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source and tutorial by [Tyler McGinnis](https://developers.google.com/experts/people/tyler-mcginnis) available at: https://tylermcginnis.com/reactjs-tutorial-a-comprehensive-guide-to-building-apps-with-react/
